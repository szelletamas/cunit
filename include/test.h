/*
 * test.h
 *
 *  Created on: 2021. m�rc. 6.
 *      Author: Kicsigamer
 */

#ifndef INCLUDE_TEST_H_
#define INCLUDE_TEST_H_

void init_test();

void deinit_test();

void rectangle_init_test();

void list_push_test_first();

void list_push_test_second();

void remove_last_test_first();

void remove_from_more_item_test();

#endif /* INCLUDE_TEST_H_ */
