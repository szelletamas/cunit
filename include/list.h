/*
 * list.h
 *
 *  Created on: 2021. m�rc. 6.
 *      Author: Kicsigamer
 */

#ifndef INCLUDE_LIST_H_
#define INCLUDE_LIST_H_

typedef struct rectangle_t{
    int a;
    int b;
    int area;
    int perimeter;
}rectangle_t;

typedef struct node_t{
    rectangle_t data;
    struct node_t* next;
    int id;
}node_t;

typedef struct linked_list_t{
    node_t* head;
    int size;
}linked_list_t;

void init_list(linked_list_t* list);
void deinit_list(linked_list_t* list);

void list_push(linked_list_t* list,rectangle_t rectangle);

rectangle_t rectangle_init(int a, int b);

void print_list(linked_list_t list);
void remove_last(linked_list_t* list);

#endif /* INCLUDE_LIST_H_ */
