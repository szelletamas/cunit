#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <CUnit/Basic.h>
#include <list.h>

#define TEST

#ifdef TEST
#include <test.h>
#endif


void process();
void test();

int main()
{

	#ifdef TEST
		test();
	#else
		process();
	#endif

		return 0;
}

void test() {
	CU_pSuite suite =NULL;
	if (CUE_SUCCESS!=CU_initialize_registry())
		return;

	suite = CU_add_suite("lista", 0, 0);
	if (NULL==suite) {
		CU_cleanup_registry();
		goto exit;
	}

	if ( (NULL==CU_add_test(suite, "rectangle initialization test", rectangle_init_test)) ||
		 (NULL==CU_add_test(suite, "If you have an existing item and you push another then the function should put the new item after the existing item.", list_push_test_second)) ||
		 (NULL==CU_add_test(suite, "push first item test", list_push_test_first)) ||
		 (NULL==CU_add_test(suite, "when the list have only one item and the remove_last function is called then the list will be empty", remove_last_test_first)) ||
		 (NULL==CU_add_test(suite, "...", remove_from_more_item_test)) ||
		 (NULL==CU_add_test(suite, "init test", init_test)) ||
		 (NULL==CU_add_test(suite, "deinit test", deinit_test)) ) {
		goto exit;
	}


	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
exit:
	CU_cleanup_registry();
}

void process(){
    linked_list_t list;

    init_list(&list);

    list_push(&list,rectangle_init(2,3));
    list_push(&list,rectangle_init(2,3));

    remove_last(&list);

    print_list(list);
    deinit_list(&list);
}

