/*
 * test.c
 *
 *  Created on: 2021. m�rc. 6.
 *      Author: Kicsigamer
 */
#include <list.h>
#include <CUnit/Basic.h>
#include <stdlib.h>

void init_test(){
	linked_list_t list;
	init_list(&list);
	CU_ASSERT_EQUAL(list.size,0);
	CU_ASSERT_PTR_NULL(list.head);
}

void deinit_test(){
	linked_list_t list;
	deinit_list(&list);
	CU_ASSERT_EQUAL(list.size,0);
	CU_ASSERT_PTR_NULL(list.head);
	//todo
}

void rectangle_init_test(){
	rectangle_t rectangle =rectangle_init(3, 4);
	CU_ASSERT_EQUAL(rectangle.a,3);
	CU_ASSERT_EQUAL(rectangle.b,4);
	CU_ASSERT_EQUAL(rectangle.area,12);
	CU_ASSERT_EQUAL(rectangle.perimeter,14);
}

void list_push_test_first(){
	linked_list_t list;
	init_list(&list);
	list_push(&list, rectangle_init(3, 4));

	CU_ASSERT_EQUAL(list.size,1);
	CU_ASSERT_PTR_NOT_NULL(list.head);

	CU_ASSERT_PTR_NULL(list.head->next);
	CU_ASSERT_EQUAL(list.head->id,0);

	CU_ASSERT_EQUAL(list.head->data.a,3);
	CU_ASSERT_EQUAL(list.head->data.b,4);
	CU_ASSERT_EQUAL(list.head->data.area,12);
	CU_ASSERT_EQUAL(list.head->data.perimeter,14);

	deinit_list(&list);
}

void list_push_test_second(){
	linked_list_t list;
	init_list(&list);
	list_push(&list, rectangle_init(3, 4));
	list_push(&list, rectangle_init(5, 2));

	CU_ASSERT_EQUAL(list.size,2);
	CU_ASSERT_PTR_NOT_NULL(list.head->next);

	CU_ASSERT_PTR_NULL(list.head->next->next);
	CU_ASSERT_EQUAL(list.head->next->id,1);

	CU_ASSERT_EQUAL(list.head->next->data.a,5);
	CU_ASSERT_EQUAL(list.head->next->data.b,2);
	CU_ASSERT_EQUAL(list.head->next->data.area,10);
	CU_ASSERT_EQUAL(list.head->next->data.perimeter,14);

	deinit_list(&list);
}

void remove_last_test_first(){
	linked_list_t list;
	init_list(&list);
	list_push(&list, rectangle_init(3, 4));

	remove_last(&list);

	CU_ASSERT_EQUAL(list.size,0);
	CU_ASSERT_PTR_NULL(list.head);

	deinit_list(&list);

}

void remove_from_more_item_test(){
	linked_list_t list;
	init_list(&list);
	list_push(&list, rectangle_init(3, 4));
	list_push(&list, rectangle_init(5, 2));

	remove_last(&list);

	CU_ASSERT_EQUAL(list.size,1);

	node_t* last_node=list.head;

	while(last_node->next!=NULL){
		last_node=last_node->next;
	}

	CU_ASSERT_EQUAL(last_node->id,0);


	deinit_list(&list);
}
