/*
 * list.c
 *
 *  Created on: 2021. m�rc. 6.
 *      Author: Kicsigamer
 */
#include <stdio.h>
#include <stdlib.h>
#include <list.h>


void init_list(linked_list_t* list){
    if(!list) return;

    list->head=NULL;
    list->size=0;
}

void deinit_list(linked_list_t* list){
    if(!list) return;

    node_t* temp= list->head;
    node_t* actual= list->head;

    list->head=NULL;
    list->size=0;

    while(actual!=NULL){
        actual=actual->next;
        free(temp);
        temp=actual;
    }
}

void list_push(linked_list_t* list,rectangle_t rectangle){
    if(!list) return;

    node_t* new_item=(node_t*) malloc(sizeof(node_t));

    new_item->data=rectangle;
    new_item->id=list->size;
    new_item->next=NULL;


    if(list->size==0) {
        list->head=new_item;
        list->size++;
        return;
    }
    list->size++;

    node_t* last_item=list->head;

    while(last_item->next!=NULL){
        last_item=last_item->next;
    }

    last_item->next=new_item;

}

rectangle_t rectangle_init(int a, int b){
    rectangle_t rectangle;
    rectangle.a=a;
    rectangle.b=b;
    rectangle.area=a*b;
    rectangle.perimeter=2*(a+b);
    return rectangle;
}

void print_list(linked_list_t list){
    node_t* last_item=list.head;
    while(last_item!=NULL){
        printf("lista %d-edik eleme: a: %d\tb: %d\tkerulet: %d\tterulet: %d\n",
               last_item->id,
               last_item->data.a,
               last_item->data.b,
               last_item->data.perimeter,
               last_item->data.area);
        last_item=last_item->next;
    }
}

void remove_last(linked_list_t* list){
    if(!list || !list->head){
        return;
    }

    if(list->head->next==NULL){
        free(list->head);
        list->head=NULL;
        list->size=0;
        return;
    }

    node_t* before_last= list->head;

    while(before_last->next->next!=NULL){
        before_last=before_last->next;
    }

    free(before_last->next);
    before_last->next=NULL;
    list->size--;

}
