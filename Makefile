# Makefile for cunit





PROJECT=cunit

DEBUG=yes

CFLAGS= -Wall -pedantic -std=c11 -D_DEFAULT_SOURCE

INCLUDES=include

BIN=bin
SRC=src

ifeq ($(DEBUG),yes) 
CFLAGS+=-g
endif

LIBS=-l cunit -l pthread

OBJECTS=$(patsubst $(SRC)/%.c,$(BIN)/%.o, $(wildcard $(SRC)/*.c)) 

$(BIN)/$(PROJECT): $(BIN) $(OBJECTS)
		$(CC) -o $(BIN)/$(PROJECT) $(OBJECTS) $(LIBS) $(CFLAGS)
		
$(BIN):
		mkdir $(BIN)
		
$(BIN)/%.o:$(SRC)/%.c
		$(CC) -o $@ -c $< -I $(INCLUDES) $(CFLAGS)

clean:
		$(RM) -r $(BIN)

all: clean $(BIN)/$(PROJECT)

test: all
		$(BIN)/$(PROJECT)

.PHONY: clean

